﻿using System.Collections.Generic;
using System.Linq;
using Data;
using UnityEngine;

namespace StaticData
{
    public class StaticDataService : IStaticDataService
    {
        private const string StaticDataLevelsPath = "StaticData";
        private readonly List<BaseData> baseData;

        public StaticDataService()
        {
            baseData = Resources.LoadAll<BaseData>(StaticDataLevelsPath).ToList();
        }

        public T GetData<T>() where T : BaseData => (T) baseData.Find(x => x is T);
    }
}