﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Ship;
using Ship.Weapon;
using UnityEngine;

namespace StaticData
{
    [CreateAssetMenu(fileName = "UpgradeSo", menuName = "ScriptableObjects/UpgradeSo")]
    public class UpgradesSo : BaseData
    {
        public List<ShipUpgradeConfig> ShipUpgradeConfigs;
        public List<CanonUpgradeConfig> CanonUpgradeConfigs;
        public List<ShootSpeedUpgradeConfig> ShootSpeedUpgradeConfigs;
        public List<BulletTimeUpgradeConfig> BulletTimeUpgradeConfigs;

        public ShipUpgradeConfig GetShipUpgradeConfigByLevel(int level) => ShipUpgradeConfigs.FirstOrDefault(config => config.Level == level);
        public CanonUpgradeConfig GetCanonUpgradeConfigByLevel(int level) => CanonUpgradeConfigs.FirstOrDefault(config => config.Level == level);
        public ShootSpeedUpgradeConfig GetShootSpeedUpgradeConfigByLevel(int level) => ShootSpeedUpgradeConfigs.FirstOrDefault(config => config.Level == level);
        public BulletTimeUpgradeConfig GetBulletTimeConfigByLevel(int level) => BulletTimeUpgradeConfigs.FirstOrDefault(config => config.Level == level);
        
        public int GetMaxShipLevel() => ShipUpgradeConfigs.Count - 1;
        public int GetMaxCanonLevel() => CanonUpgradeConfigs.Count - 1;
        public int GetMaxShootSpeedLevel() => ShootSpeedUpgradeConfigs.Count - 1;
        public int GetMaxBulletTimeAliveLevel() => BulletTimeUpgradeConfigs.Count - 1;
    }

    [Serializable]
    public class BaseConfig
    {
        public int Level;
        public int Price;
    }

    [Serializable]
    public class ShipUpgradeConfig : BaseConfig
    {
        public int Health;
        public ShipController ShipController;
    }

    [Serializable]
    public class CanonUpgradeConfig : BaseConfig
    {
        public Canon Canon;
    }
    
    [Serializable]
    public class ShootSpeedUpgradeConfig : BaseConfig
    {
        public float ShootDelay;
    }

    [Serializable]
    public class BulletTimeUpgradeConfig : BaseConfig
    {
        public float BulletTimeAlive;
    }
}