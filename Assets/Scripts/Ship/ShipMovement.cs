﻿using Dreamteck.Splines;
using Game;
using Infrastructure.Input;
using Infrastructure.Services;
using UnityEngine;

namespace Ship
{
    public class ShipMovement : MonoBehaviour
    {
        public bool CanMove { get; set; } = false;
        
        private IInputService _inputService;

        private void OnEnable() => _inputService.OnEventDrag += ChangeHorizontalCoords;
        private void OnDisable() => _inputService.OnEventDrag -= ChangeHorizontalCoords;

        private void Awake() => _inputService = AllServices.Container.Single<IInputService>();

        private void ChangeHorizontalCoords(float value)
        {
            if(!CanMove)
                return;
            
            var xPos = transform.localPosition.x;
            var newValue = Mathf.Clamp(value, -1f, 1f);
            
            if (newValue >= 0 && xPos < 4f)
                transform.localPosition = new Vector3(xPos + newValue / 5f, 0, 0);
            else if (newValue < 0 && xPos > -4f)
                transform.localPosition = new Vector3(xPos + newValue / 5f, 0, 0);
        }
    }
}