﻿using Components;
using UnityEngine;

namespace Ship.Weapon
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private TrailRenderer _trailRenderer;
        [SerializeField] private float _speed;
        [SerializeField] private int _damage;
        [field:SerializeField] public float TimeToDestroy { get; set; }
        
        private float _currentTime;
        private bool _isMove = false;
        private Vector3 _direction;
        
        private void Update()
        {
            if(!_isMove)
                return;
            
            transform.Translate(_direction * _speed * Time.deltaTime);

            _currentTime += Time.deltaTime;
            if (_currentTime >= TimeToDestroy)
                DisableBullet();
        }
        
        public void StartMove()
        {
            _direction = Vector3.forward;
            _currentTime = 0f;
            _isMove = true;
        }
        
        public void StartMove(Vector3 direction)
        {
            _direction = direction;
            _currentTime = 0f;
            _isMove = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IDamageable damageable))
            {
                damageable.GetDamage(_damage);
                DisableBullet();
            }
        }

        private void DisableBullet()
        {
            _isMove = false;
            gameObject.SetActive(false);
            _trailRenderer.Clear();
        }
    }
}