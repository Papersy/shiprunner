﻿using System.Collections.Generic;
using UnityEngine;

namespace Ship.Weapon
{
    public class BossCanon : MonoBehaviour
    {
        [SerializeField] private Transform[] _shootPoint;
        [SerializeField] private Bullet _bulletPrefab;

        public bool CanShoot { get; set; }
        [field:SerializeField] public float ShootDelay { get; set; }
        [field:SerializeField] public float BulletAliveTime { get; set; }
        
        private float _currentTime;
        private List<Bullet> _bulletsPool = new();

        private void Awake() => GenerateBulletsPool();

        private void Update()
        {
            if(!CanShoot)
                return;
            if (_currentTime >= ShootDelay)
            {
                Shoot();
                _currentTime = 0f;
            }

            _currentTime += Time.deltaTime;
        }

        public void StartShoot()
        {
            _currentTime = ShootDelay;
            CanShoot = true;
        }

        public void StopShoot() => CanShoot = false;

        private void Shoot()
        {
            foreach (var point in _shootPoint)
            {
                var bulletPos = point.transform.position;
                var bullet = GetFreeBullet();

                bullet.TimeToDestroy = BulletAliveTime;
                bullet.transform.position = bulletPos;
                bullet.StartMove(Vector3.back);
            }
        }
        
        private void GenerateBulletsPool()
        {
            for (int i = 0; i < 50; i++)
            {
                var bullet = Instantiate(_bulletPrefab);
                bullet.gameObject.SetActive(false);
                _bulletsPool.Add(bullet);
            }
        }

        private Bullet GetFreeBullet()
        {
            foreach (var bullet in _bulletsPool)
            {
                if (!bullet.gameObject.activeSelf)
                {
                    bullet.gameObject.SetActive(true);
                    return bullet;
                }
            }
            
            var newBullet = Instantiate(_bulletPrefab);
            _bulletsPool.Add(newBullet);

            return newBullet;
        }
    }
}