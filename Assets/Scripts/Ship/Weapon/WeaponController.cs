﻿using Data;
using Game;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using StaticData;
using UnityEngine;

namespace Ship.Weapon
{
    public class WeaponController : MonoBehaviour
    {
        [SerializeField] private Transform _spawnPoint;

        private Canon _currentCanon;
        private PlayerResourcesData _playerData;

        private void Awake()
        {
            _playerData = AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData;

            var currentCanonLevel = _playerData.CanonLevel;
            InitCanon(currentCanonLevel);
        }

        private void OnEnable()
        {
            _playerData.OnCanonUpdate += OnCanonUpdate;
            _playerData.OnShootSpeedUpdate += OnShootSpeedUpdate;
            _playerData.OnBulletTimeUpdate += OnBulletTimeUpdate;
        }
        private void OnDisable()
        {
            _playerData.OnCanonUpdate -= OnCanonUpdate;
            _playerData.OnShootSpeedUpdate -= OnShootSpeedUpdate;
            _playerData.OnBulletTimeUpdate -= OnBulletTimeUpdate;
        }

        private void InitCanon(int level)
        {
            var upgradesSo = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>();
            var canonConfig = upgradesSo.GetCanonUpgradeConfigByLevel(level);

            if(_currentCanon != null)
                Destroy(_currentCanon.gameObject);
            
            var prefab = canonConfig.Canon;
            _currentCanon = Instantiate(prefab, transform);
            _currentCanon.transform.position = _spawnPoint.position;
            
            UpdateCanonParameters();
        }

        private void UpdateCanonParameters()
        {
            var bulletTimeAliveLevel = _playerData.BulletTimeLevel;
            var shootTimeSpeedLevel = _playerData.ShootSpeedLevel;

            var bulletTimeConfig = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>()
                .GetBulletTimeConfigByLevel(bulletTimeAliveLevel);
            var shootTimeConfig = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>()
                .GetShootSpeedUpgradeConfigByLevel(shootTimeSpeedLevel);

            _currentCanon.ShootDelay = shootTimeConfig.ShootDelay;
            _currentCanon.BulletAliveTime = bulletTimeConfig.BulletTimeAlive;
        }

        private void OnCanonUpdate(int level) => InitCanon(level);

        private void OnBulletTimeUpdate(int level) => UpdateCanonParameters();
        private void OnShootSpeedUpdate(int level) => UpdateCanonParameters();

        public void StartGame() => _currentCanon.StartShoot();
        public void StopGame() => _currentCanon.StopShoot();
    }
}