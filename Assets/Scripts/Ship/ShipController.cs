﻿using Dreamteck.Splines;
using Game;
using Ship.Weapon;
using UnityEngine;

namespace Ship
{
    public class ShipController : MonoBehaviour
    {
        [SerializeField] private ShipMovement _shipMovement;
        [SerializeField] private WeaponController _weaponController;

        private void OnEnable()
        {
            GameController.Instance.OnGameStart += OnGameStart;
            GameController.Instance.OnLost += OnGameStop;
            GameController.Instance.OnVictory += OnGameStop;
        }

        private void OnDisable()
        {
            GameController.Instance.OnGameStart -= OnGameStart;
            GameController.Instance.OnLost -= OnGameStop;
            GameController.Instance.OnVictory -= OnGameStop;
        }

        private void OnGameStart()
        {
            _shipMovement.CanMove = true;
            _weaponController.StartGame();
        }

        private void OnGameStop()
        {
            _shipMovement.CanMove = false;
            _weaponController.StopGame();
        }
    }
}