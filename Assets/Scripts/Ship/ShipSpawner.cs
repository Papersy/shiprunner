﻿using Dreamteck.Splines;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using StaticData;
using UnityEngine;

namespace Ship
{
    public class ShipSpawner : MonoBehaviour
    {
        private ShipController _currentShipController;
        
        private void OnEnable() => AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.OnShipUpdate += OnShipUpgrade;
        private void OnDisable() => AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.OnShipUpdate -= OnShipUpgrade;

        private void Awake()
        {
            var currentShipLevel = AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.ShipLevel;
            InitShip(currentShipLevel);
        }

        private void InitShip(int level)
        {
            var shipConfig = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>()
                .GetShipUpgradeConfigByLevel(level);

            if(_currentShipController != null)
                Destroy(_currentShipController.gameObject);
            
            _currentShipController = Instantiate(shipConfig.ShipController, transform);
        }

        private void OnShipUpgrade(int level) => InitShip(level);
    }
}