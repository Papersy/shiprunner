﻿using Components;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using UnityEngine;

namespace Ship
{
    public class ShipCollisionDetector : MonoBehaviour, IDamageable
    {
        public void GetDamage(int value) => AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.MinusHealth(value);
    }
}