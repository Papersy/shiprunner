using System;
using Infrastructure.Services;
using UnityEngine.EventSystems;

namespace Infrastructure.Input
{
    public interface IInputService : IService, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        public Action OnEventDown { get; set; }
        public Action OnEventUp { get; set; }
        public Action<float> OnEventDrag { get; set; }
    }
}