using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Infrastructure.Input
{
    public class InputService : MonoBehaviour, IInputService
    {
        public Action OnEventDown { get; set; }
        public Action OnEventUp { get; set; }
        public Action<float> OnEventDrag { get; set; }

        private Vector2 _prevPos;
        private Vector2 _currentPos;

        public void OnPointerDown(PointerEventData eventData)
        {
            _prevPos = UnityEngine.Input.mousePosition;
            _currentPos = UnityEngine.Input.mousePosition;
            OnEventDown?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnEventUp?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            var mousePos = UnityEngine.Input.mousePosition;

            _prevPos = _currentPos;
            _currentPos = mousePos;

            var x = _currentPos.x - _prevPos.x;
            
            OnEventDrag?.Invoke(x);
        }

    }
}