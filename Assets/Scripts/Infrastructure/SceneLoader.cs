﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Infrastructure
{
    public class SceneLoader
    {
        private readonly ICoroutineRunner _coroutineRunner;
        private readonly HUDLoading hudLoading;

        public SceneLoader(ICoroutineRunner coroutineRunner, HUDLoading hudLoading)
        {
            this.hudLoading = hudLoading;
            _coroutineRunner = coroutineRunner;
        }

        public void Load(string name, Action onLoaded = null) =>
            _coroutineRunner.StartCoroutine(LoadScene(name, onLoaded));

        private IEnumerator LoadScene(string nextSceneName, Action onLoaded = null)
        {
            Debug.Log($"Next {nextSceneName} , current {SceneManager.GetActiveScene().name}");

            if (nextSceneName == SceneManager.GetActiveScene().name)
            {
                onLoaded?.Invoke();
                yield break;
            }

            AsyncOperation waitNextScene = SceneManager.LoadSceneAsync(nextSceneName);
            hudLoading.StartLoad(waitNextScene, () => { });

            while (!waitNextScene.isDone)
                yield return null;

            onLoaded?.Invoke();
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(nextSceneName));
        }
    }
}