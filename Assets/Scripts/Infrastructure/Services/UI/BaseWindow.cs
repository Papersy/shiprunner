﻿using CodeBase.StaticData;
using Data;
using Infrastructure.Services.Data;
using UnityEngine;

namespace Infrastructure.Services.UI
{
    public class BaseWindow : MonoBehaviour
    {
        [SerializeField] private bool hideOnStart = true;
        [HideInInspector] public PlayerDynamicData PlayerDynamicData;
        
        public void Awake()
        {
            if (hideOnStart)
                Hide();

            PlayerDynamicData = AllServices.Container.Single<IDataService>().PlayerDynamicData;
            
            InitWindow();
        }
        
        public void Show()
        {
            if (gameObject.activeSelf) return;
            gameObject.SetActive(true);

            OnShow();
        }

        public void Hide()
        {
            if (!gameObject.activeSelf) return;
            gameObject.SetActive(false);

            OnHide();
        }
        
        public virtual void InitWindow()
        {
            
        }
        
        public virtual void OnShow()
        {
        }

        public virtual void OnHide()
        {
        }
    }
}