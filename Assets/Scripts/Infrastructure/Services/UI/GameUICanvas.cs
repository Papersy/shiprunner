﻿using UI;
using UnityEngine;

namespace Infrastructure.Services.UI
{
    public class GameUICanvas : MonoBehaviour
    {
        public static GameUICanvas Instance;

        public LevelFinishWindow LevelFinishWindow;
        public UpgradeWindow UpgradeWindow;
        public StatisticWindow StatisticWindow;

        public void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }
    }
}