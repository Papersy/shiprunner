using System.Collections.Generic;
using Data;
using UnityEngine;

namespace Infrastructure.Services.Data
{
    public class DataService : IDataService
    {
        public PlayerDynamicData PlayerDynamicData => _playerDynamicData;

        private readonly PlayerDynamicData _playerDynamicData;

        private readonly List<ISaveProgress> _savers = new List<ISaveProgress>();

        public DataService()
        {
            LoadOrCreateData(out _playerDynamicData, KeyStaticData.PlayerDynamicDataKey);
        }

        public void RegisterSaver(ISaveProgress saver) =>
            _savers.Add(saver);

        public void UnregisterSaver(ISaveProgress saver) =>
            _savers.Remove(saver);

        public void SaveProgress()
        {
            SaveData(_playerDynamicData, KeyStaticData.PlayerDynamicDataKey);

            foreach (ISaveProgress saver in _savers)
                saver.SaveProgress();

            PlayerPrefs.Save();
        }

        private static void LoadOrCreateData<T>(out T value, string location) where T : new()
        {
            string data = PlayerPrefs.GetString(location);
            value = data != "" ? JsonUtility.FromJson<T>(data) : new T();
        }

        private static void SaveData<T>(T value, string location, bool show = false)
        {
            if (show) Debug.Log(JsonUtility.ToJson(value));
            PlayerPrefs.SetString(location, JsonUtility.ToJson(value));
        }
    }
}