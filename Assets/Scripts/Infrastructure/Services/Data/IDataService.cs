using Data;

namespace Infrastructure.Services.Data
{
    public interface IDataService : IService
    {
        PlayerDynamicData PlayerDynamicData { get; }

        void RegisterSaver(ISaveProgress saver);
        void SaveProgress();
        void UnregisterSaver(ISaveProgress saver);
    }
}