﻿using Infrastructure.Services;
using UnityEngine;

namespace Infrastructure.Factory
{
    public interface IGameFactory : IService
    {
        void Cleanup();
        GameObject Instantiate(string prefabPath);
        GameObject InstantiateHero(string prefabPath);
    }
}