﻿using System;
using System.Threading.Tasks;
using Game;
using Infrastructure;
using Infrastructure.Services;
using Infrastructure.Services.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class LevelFinishWindow : BaseWindow
    {
        [SerializeField] private GameObject _lostPanel;
        [SerializeField] private GameObject _victoryPanel;
        [SerializeField] private Button _restart;
        [SerializeField] private Button _next;

        private void OnEnable()
        {
            _restart.onClick.AddListener(RestartLevel);
            _next.onClick.AddListener(RestartLevel);
        }

        private void OnDisable()
        {
            _restart.onClick.RemoveListener(RestartLevel);
            _next.onClick.RemoveListener(RestartLevel);
        }
        
        public void VictoryGame()
        {
            Show();
            _lostPanel.SetActive(false);
            _victoryPanel.SetActive(true);
        }

        public void LostGame()
        {
            Show();
            _lostPanel.SetActive(true);
            _victoryPanel.SetActive(false);
        }

        private void RestartLevel() => ResetLevel();

        private async Task ResetLevel()
        {
            await GameController.Instance.ResetGame();
        }
    }
}