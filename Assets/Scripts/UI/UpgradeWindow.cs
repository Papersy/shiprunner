﻿using System;
using Data;
using Game;
using Infrastructure.Services;
using Infrastructure.Services.UI;
using StaticData;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UpgradeWindow : BaseWindow
    {
        [SerializeField] private UpgradeBtn _shipUpgrade;
        [SerializeField] private UpgradeBtn _weaponUpgrade;
        [SerializeField] private UpgradeBtn _bulletDelayUpgrade;
        [SerializeField] private UpgradeBtn _shootTimeUpgrade;
        [SerializeField] private Button[] _closeButtons;

        private void OnEnable()
        {
            foreach (var button in _closeButtons)
            {
                button.onClick.AddListener(Hide);
                button.onClick.AddListener(() => GameController.Instance.StartGame());
            }
        }

        private void OnDisable()
        {
            foreach (var button in _closeButtons)
                button.onClick.RemoveAllListeners();
        }

        public override void OnShow() => UpdateUI();

        public override void InitWindow()
        {
            var upgradesSo = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>();

            AddShipListeners(upgradesSo);
            AddWeaponListeners(upgradesSo);
            AddBulletListeners(upgradesSo);
            AddShootTimeListeners(upgradesSo);
        }

        private void UpdateUI()
        {
            UpdateShipUI();
            UpdateWeaponUI();
            UpdateBulletDelayUI();
            UpdateShootSpeedUI();
        }

        private void UpdateShipUI()
        {
            var upgradeSo = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>();
            var currentShipLevel = PlayerDynamicData.PlayerResourcesData.ShipLevel;
            if (currentShipLevel >= upgradeSo.GetMaxShipLevel())
            {
                _shipUpgrade.UpdateUI(ConstantsData.MaxLevel);
                return;
            }

            var config = upgradeSo.GetShipUpgradeConfigByLevel(currentShipLevel + 1);
            _shipUpgrade.UpdateUI(ConstantsData.ShipUpgrade, config.Price, config.Level);
        }

        private void AddShipListeners(UpgradesSo upgradesSo)
        {
            var currentShipLevel = PlayerDynamicData.PlayerResourcesData.ShipLevel;
            
            var price = upgradesSo.GetShipUpgradeConfigByLevel(currentShipLevel).Price;
            _shipUpgrade.Button.onClick.AddListener(() =>
            {
                var shipLevel = PlayerDynamicData.PlayerResourcesData.ShipLevel;
                if (shipLevel >= upgradesSo.GetMaxShipLevel())
                    return;
                
                if (PlayerDynamicData.PlayerResourcesData.TryShipUpgrade(price))
                {
                    PlayerDynamicData.PlayerResourcesData.MaxHealth = upgradesSo.GetShipUpgradeConfigByLevel(currentShipLevel).Health;
                    PlayerDynamicData.PlayerResourcesData.ResetHealth();
                    UpdateShipUI();
                }
            });
        }

        private void UpdateWeaponUI()
        {
            var upgradeSo = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>();

            var currentWeaponLevel = PlayerDynamicData.PlayerResourcesData.CanonLevel;
            if (currentWeaponLevel >= upgradeSo.GetMaxCanonLevel())
            {
                _weaponUpgrade.UpdateUI(ConstantsData.MaxLevel);
                return;
            }

            var config = upgradeSo.GetCanonUpgradeConfigByLevel(currentWeaponLevel + 1);
            _weaponUpgrade.UpdateUI(ConstantsData.WeaponUpgrade, config.Price, config.Level);
        }

        private void AddWeaponListeners(UpgradesSo upgradesSo)
        {
            var currentWeaponLevel = PlayerDynamicData.PlayerResourcesData.CanonLevel;
            
            var price = upgradesSo.GetCanonUpgradeConfigByLevel(currentWeaponLevel).Price;
            _weaponUpgrade.Button.onClick.AddListener(() =>
            {
                var weaponLevel = PlayerDynamicData.PlayerResourcesData.CanonLevel;
                if (weaponLevel >= upgradesSo.GetMaxCanonLevel())
                    return;
                
                if (PlayerDynamicData.PlayerResourcesData.TryCanonUpgrade(price))
                    UpdateWeaponUI();
            });
        }

        private void UpdateBulletDelayUI()
        {
            var upgradeSo = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>();

            var currentBulletDelayLevel = PlayerDynamicData.PlayerResourcesData.BulletTimeLevel;
            if (currentBulletDelayLevel >= upgradeSo.GetMaxBulletTimeAliveLevel())
            {
                _bulletDelayUpgrade.UpdateUI(ConstantsData.MaxLevel);
                return;
            }

            var config = upgradeSo.GetBulletTimeConfigByLevel(currentBulletDelayLevel + 1);
            _bulletDelayUpgrade.UpdateUI(ConstantsData.BulletTimeAliveUpgrade, config.Price, config.Level);
        }

        private void AddBulletListeners(UpgradesSo upgradesSo)
        {
            var currentBulletLevel = PlayerDynamicData.PlayerResourcesData.BulletTimeLevel;
            
            var price = upgradesSo.GetBulletTimeConfigByLevel(currentBulletLevel).Price;
            _bulletDelayUpgrade.Button.onClick.AddListener(() =>
            {
                var bulletLevel = PlayerDynamicData.PlayerResourcesData.BulletTimeLevel;
                if (bulletLevel >= upgradesSo.GetMaxBulletTimeAliveLevel())
                    return;
                
                if (PlayerDynamicData.PlayerResourcesData.TryBulletTimeUpgrade(price))
                    UpdateBulletDelayUI();
            });
        }

        private void UpdateShootSpeedUI()
        {
            var upgradeSo = AllServices.Container.Single<IStaticDataService>().GetData<UpgradesSo>();

            var currentShootSpeedLevel = PlayerDynamicData.PlayerResourcesData.ShootSpeedLevel;
            if (currentShootSpeedLevel >= upgradeSo.GetMaxShootSpeedLevel())
            {
                _shootTimeUpgrade.UpdateUI(ConstantsData.MaxLevel);
                return;
            }

            var config = upgradeSo.GetShootSpeedUpgradeConfigByLevel(currentShootSpeedLevel + 1);
            _shootTimeUpgrade.UpdateUI(ConstantsData.ShootSpeed, config.Price, config.Level);
        }

        private void AddShootTimeListeners(UpgradesSo upgradesSo)
        {
            var currentBulletLevel = PlayerDynamicData.PlayerResourcesData.ShootSpeedLevel;
            
            var price = upgradesSo.GetShootSpeedUpgradeConfigByLevel(currentBulletLevel).Price;
            _shootTimeUpgrade.Button.onClick.AddListener(() =>
            {
                var bulletLevel = PlayerDynamicData.PlayerResourcesData.ShootSpeedLevel;
                if (bulletLevel >= upgradesSo.GetMaxShootSpeedLevel())
                    return;
                
                if (PlayerDynamicData.PlayerResourcesData.TryShootSpeedUpgrade(price))
                    UpdateShootSpeedUI();
            });
        }
    }
}