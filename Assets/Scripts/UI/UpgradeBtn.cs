﻿using Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UpgradeBtn : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _desc;
        [SerializeField] private TextMeshProUGUI _price;
        [SerializeField] private TextMeshProUGUI _level;
        
        public Button Button;

        public void UpdateUI(string desc)
        {
            _desc.text = desc;
            _price.text = ConstantsData.Max;
            _level.text = $"Level: {ConstantsData.Max}";
        }
        
        public void UpdateUI(string desc, int price, int level)
        {
            _desc.text = desc;
            _price.text = price.ToString();
            _level.text = $"Level: {level}";
        }
    }
}