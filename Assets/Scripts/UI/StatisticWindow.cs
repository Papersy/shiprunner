﻿using Infrastructure.Services.UI;
using TMPro;
using UnityEngine;

namespace UI
{
    public class StatisticWindow : BaseWindow
    {
        [SerializeField] private TextMeshProUGUI _coins;
        [SerializeField] private TextMeshProUGUI _health;

        private void OnEnable()
        {
            PlayerDynamicData.PlayerResourcesData.OnHealthUpdate += HealthUpdate;
            PlayerDynamicData.PlayerResourcesData.OnCoinsUpdate += CoinsUpdate;
        }

        private void OnDisable()
        {
            PlayerDynamicData.PlayerResourcesData.OnHealthUpdate -= HealthUpdate;
            PlayerDynamicData.PlayerResourcesData.OnCoinsUpdate -= CoinsUpdate;
        }

        private void CoinsUpdate(int value) => _coins.text = value.ToString();
        private void HealthUpdate(int value) => _health.text = value.ToString();

        public override void InitWindow()
        {
            _coins.text = PlayerDynamicData.PlayerResourcesData.Coins.ToString();
            _health.text = PlayerDynamicData.PlayerResourcesData.CurrentHealth.ToString();
        }
    }
}