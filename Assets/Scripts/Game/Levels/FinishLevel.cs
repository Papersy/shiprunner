﻿using UnityEngine;

namespace Game.Levels
{
    public class FinishLevel : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("PlayerCollision"))
                GameController.Instance.VictoryGame();
        }
    }
}