﻿using Dreamteck.Splines;
using Game.Obstacles;
using Ship.Weapon;
using UnityEngine;

namespace Game.Levels
{
    public class BossLevel : BaseLevel
    {
        [SerializeField] private SplineFollower _bossFollower;
        [SerializeField] private Boss _boss;
        [SerializeField] private BossCanon _canon;
        
        private void OnEnable() => GameController.Instance.OnGameStart += StartGame;
        private void OnDisable() => GameController.Instance.OnGameStart -= StartGame;
        
        private void StartGame()
        {
            _bossFollower.SetPercent(0.01f);
            _bossFollower.follow = true;
            _canon.StartShoot();
            _boss.ResetBoss();
        }
    }
}