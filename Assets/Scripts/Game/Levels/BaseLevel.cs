﻿using System;
using Dreamteck.Splines;
using UnityEngine;

namespace Game.Levels
{
    public class BaseLevel : MonoBehaviour
    {
        [SerializeField] private SplineComputer _splineComputer;
        
        private void Awake()
        {
            InitLevel();
        }

        public SplineComputer GetSplineComputer() => _splineComputer;
        public virtual void InitLevel(){}
    }
}