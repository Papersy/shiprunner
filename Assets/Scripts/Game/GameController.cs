﻿
using System;
using System.Threading.Tasks;
using Dreamteck.Splines;
using Game.Levels;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using Infrastructure.Services.UI;
using UnityEngine;

namespace Game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private SplineFollower[] _followers;
        [SerializeField] private BaseLevel _normalLevel;
        [SerializeField] private BaseLevel _bossLevel;

        public static GameController Instance;

        public event Action OnGameStart;
        public event Action OnLost;
        public event Action OnVictory;

        private BaseLevel _currentLevel;
        private SplineComputer _currentSplineComputer;

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }

        public async Task InitLevel()
        {
            if(_currentLevel != null)
                Destroy(_currentLevel.gameObject);
            
            if (AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerProgressData.CurrentLevel % 2 == 0)
            {
                _currentLevel = Instantiate(_normalLevel);
                _currentLevel.transform.position = Vector3.zero;
                _currentSplineComputer = _currentLevel.GetSplineComputer();
            }
            else
            {
                _currentLevel = Instantiate(_bossLevel);
                _currentSplineComputer = _currentLevel.GetSplineComputer();
            }

            InitFollowers();
            GameUICanvas.Instance.UpgradeWindow.Show();
        }

        public async Task ResetGame()
        {
            await InitLevel();
            
            GameUICanvas.Instance.LevelFinishWindow.Hide();
            GameUICanvas.Instance.UpgradeWindow.Show();
        }
        
        public void StartGame()
        {
            StartFollow();
            ResetFollowers();
            AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.ResetHealth();
            OnGameStart?.Invoke();
        }

        public void LostGame()
        {
            StopFollow();
            GameUICanvas.Instance.LevelFinishWindow.LostGame();
            OnLost?.Invoke();
        }

        public void VictoryGame()
        {
            StopFollow();
            AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerProgressData.CompleteCurrentLevel();
            GameUICanvas.Instance.LevelFinishWindow.VictoryGame();
            OnVictory?.Invoke();
        }

        private void InitFollowers()
        {
            foreach (var follower in _followers)
            {
                follower.follow = false;
                follower.spline = _currentSplineComputer;
            }
        }

        private void StartFollow()
        {
            foreach (var follower in _followers)
                follower.follow = true;
        }

        private void StopFollow()
        {
            foreach (var follower in _followers)
                follower.follow = false;
        }

        private void ResetFollowers()
        {
            foreach (var follower in _followers)
                follower.SetPercent(0f);
        }
    }
}