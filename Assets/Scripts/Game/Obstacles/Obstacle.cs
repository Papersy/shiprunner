﻿using Components;
using UnityEngine;

namespace Game.Obstacles
{
    public class Obstacle : MonoBehaviour, IDamageable
    {
        [SerializeField] private ObstacleView _obstacleView;
        [SerializeField] private int _health = 50;
        public int CollisionDamage = 15;

        private void Awake() => _obstacleView.ResetProgressBar(_health);

        public void GetDamage(int value)
        {
            _health -= value;
            _obstacleView.UpdateUI(_health);
            
            if(_health <= 0)
                Destroy(gameObject);
        }
    }
}