﻿using UnityEngine;

namespace Game.Obstacles
{
    public class TurretDetector : MonoBehaviour
    {
        [SerializeField] private Turret _turret;

        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("PlayerCollision"))
                _turret.SetTarget(other.transform);
        }

        private void OnTriggerExit(Collider other)
        {
            if(other.CompareTag("PlayerCollision"))
                _turret.SetTarget(null);
        }
    }
}