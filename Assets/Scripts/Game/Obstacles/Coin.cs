﻿using System;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using UnityEngine;

namespace Game.Obstacles
{
    public class Coin : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("PlayerCollision"))
            {
                AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.AddCoin(250);
                Destroy(gameObject);
            }
        }
    }
}