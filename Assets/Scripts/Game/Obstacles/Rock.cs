﻿using Infrastructure.Services;
using Infrastructure.Services.Data;
using UnityEngine;

namespace Game.Obstacles
{
    public class Rock : Obstacle
    {
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("PlayerCollision"))
                AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerResourcesData.MinusHealth(CollisionDamage);
        }
    }
}