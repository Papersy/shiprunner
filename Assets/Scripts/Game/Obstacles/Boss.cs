﻿using System;
using Components;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Obstacles
{
    public class Boss : MonoBehaviour, IDamageable
    {
        [SerializeField] private int _health;
        [SerializeField] private ObstacleView _obstacleView;
        [SerializeField] private Transform _meshContainer;

        private float _currentTime;
        private float _timeToChangePos = 10f;
        private bool _isChangePos = false;
        private float _newXPos;
        
        private void Update()
        {
            if (_currentTime >= _timeToChangePos && !_isChangePos)
            {
                _newXPos = Random.Range(-4f, 4f);
                _isChangePos = true;
            }

            if (!_isChangePos)
                _currentTime += Time.deltaTime;
        }

        private void FixedUpdate()
        {
            if(_isChangePos)
                ChangePos();
        }

        public void ResetBoss()
        {
            _health = 250;
            _obstacleView.ResetProgressBar(_health);
        }
        
        public void GetDamage(int value)
        {
            _health -= value;
            _obstacleView.UpdateUI(_health);

            if (_health <= 0)
            {
                GameController.Instance.VictoryGame();
                gameObject.SetActive(false);
            }
        }

        private void ChangePos()
        {
            var currentXPos = _meshContainer.transform.localPosition.x;
            var currentYPos = _meshContainer.transform.localPosition.y;
            var currentZPos = _meshContainer.transform.localPosition.z;
            
            if (currentXPos >= _newXPos)
            {
                _isChangePos = false;
                return;
            }
            
            if(currentXPos >= _newXPos)
                _meshContainer.transform.localPosition = new Vector3(currentXPos - 0.05f, currentYPos, currentZPos);
            else 
                _meshContainer.transform.localPosition = new Vector3(currentXPos + 0.05f, currentYPos, currentZPos);
        }
    }
}