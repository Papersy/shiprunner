﻿

using UnityEngine;
using UnityEngine.UI;

namespace Game.Obstacles
{
    public class ObstacleView : MonoBehaviour
    {
        [SerializeField] private Slider _healthBar;

        public void ResetProgressBar(int maxValue)
        {
            _healthBar.maxValue = maxValue;
            _healthBar.value = maxValue;
        }

        public void UpdateUI(int value) => _healthBar.value = value;
    }
}