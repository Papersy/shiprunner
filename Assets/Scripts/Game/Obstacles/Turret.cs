﻿using DG.Tweening;
using Ship.Weapon;
using UnityEngine;

namespace Game.Obstacles
{
    public class Turret : Obstacle
    {
        [SerializeField] private Bullet _bulletPrefab;
        [SerializeField] private float _shootDelay;
        [SerializeField] private GameObject _canonMesh;
        [SerializeField] private Transform _shootPoint;
        [SerializeField] private Transform _target;

        private float _currentTime;

        private void Update()
        {
            if(_target == null)
                return;
            
            Follow();
            
            if (_currentTime >= _shootDelay)
            {
                Shoot();
                _currentTime = 0f;
            }
            _currentTime += Time.deltaTime;
        }

        public void SetTarget(Transform target) => _target = target;

        private void Follow()
        {
            Vector3 direction = _target.transform.position - transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            _canonMesh.transform.rotation = Quaternion.RotateTowards(_canonMesh.transform.rotation, targetRotation, 100f * Time.deltaTime);
        }
        
        private void Shoot()
        {
            var bulletPos = _shootPoint.transform.position;
            var bullet = Instantiate(_bulletPrefab, bulletPos, _canonMesh.transform.rotation);
            var offset = new Vector3(0, -2f, 2f);
            Vector3 direction =  _shootPoint.position - _target.transform.position + offset;
            
            bullet.TimeToDestroy = 3f;
            bullet.StartMove(direction.normalized);
        }
    }
}