﻿using System;
using Game;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class PlayerResourcesData
    {
        public int Coins = 9999;
        public int CurrentHealth = 125;
        public int MaxHealth = 125;
        
        public int ShipLevel;
        public int CanonLevel;
        public int BulletTimeLevel;
        public int ShootSpeedLevel;

        public event Action<int> OnShipUpdate;
        public event Action<int> OnCanonUpdate;
        public event Action<int> OnBulletTimeUpdate;
        public event Action<int> OnShootSpeedUpdate;
        
        public event Action<int> OnCoinsUpdate;
        public event Action<int> OnHealthUpdate; 
        
        public bool TryShipUpgrade(int cost)
        {
            if (cost > Coins)
                return false;

            ShipLevel++;
            MinusCoin(cost);

            OnShipUpdate?.Invoke(ShipLevel);
            return true;
        }
        public bool TryCanonUpgrade(int cost)
        {
            if (cost > Coins)
                return false;

            CanonLevel++;
            MinusCoin(cost);

            OnCanonUpdate?.Invoke(CanonLevel);
            return true;
        }
        public bool TryBulletTimeUpgrade(int cost)
        {
            if (cost > Coins)
                return false;

            BulletTimeLevel++;
            MinusCoin(cost);

            OnBulletTimeUpdate?.Invoke(BulletTimeLevel);
            return true;
        }
        public bool TryShootSpeedUpgrade(int cost)
        {
            if (cost > Coins)
                return false;

            ShootSpeedLevel++;
            MinusCoin(cost);

            OnShootSpeedUpdate?.Invoke(ShootSpeedLevel);
            return true;
        }

        public void AddCoin(int value)
        {
            Coins += value;
            
            OnCoinsUpdate?.Invoke(Coins);
        }
        public void MinusCoin(int value)
        {
            Coins -= value;
            
            OnCoinsUpdate?.Invoke(Coins);
        }

        public void ResetHealth()
        {
            CurrentHealth = MaxHealth;
            
            OnHealthUpdate?.Invoke(CurrentHealth);
        }
        public void MinusHealth(int value)
        {
            CurrentHealth -= value;
            
            if(CurrentHealth <= 0)
                GameController.Instance.LostGame();
            
            OnHealthUpdate?.Invoke(CurrentHealth);
        }
    }
}