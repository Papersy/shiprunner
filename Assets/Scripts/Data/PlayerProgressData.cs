﻿using System;

namespace Data
{
    [Serializable]
    public class PlayerProgressData
    {
        public int CurrentLevel = 0;
        public event Action<int> OnLevelCompleted;

        public void CompleteCurrentLevel()
        {
            CurrentLevel++;
            
            OnLevelCompleted?.Invoke(CurrentLevel);
        }
    }
}