﻿namespace Data
{
    public class ConstantsData
    {
        public static float Epsilon = 0.013f;
        
        public static string HUDAddress = "UI/HUDContainer";

        public static string MaxLevel = "Max";
        public static string ShipUpgrade = "Upgrade ship level";
        public static string WeaponUpgrade = "Upgrade canon level";
        public static string BulletTimeAliveUpgrade = "Upgrade bullet fly time";
        public static string ShootSpeed = "Upgrade shoot speed";
        
        public static string Max = "Max";
    }
}