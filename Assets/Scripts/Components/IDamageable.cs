﻿namespace Components
{
    public interface IDamageable
    {
        public void GetDamage(int value);
    }
}